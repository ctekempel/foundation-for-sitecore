﻿namespace FoundationForSitecore.Models
{
    using System.Web;

    public class OrbitSlideViewModel
    {
        public string ImageSrc { get; set; }

        public string ImageAlt { get; set; }

        public string Caption { get; set; }

        public ItemWrapper Item { get; set; }

        public string ImageFieldName { get; set; }

        public string CaptionFieldName { get; set; }

        public HtmlString Content { get; set; }

        public string ContentFieldName { get; set; }

        public string DeepLinkTarget { get; set; }
    }
}