﻿namespace FoundationForSitecore.Models
{
    public class FieldViewModel
    {
        public FieldViewModel(string fieldName, ItemWrapper item)
        {
            this.Item = item;
            this.FieldName = fieldName;
        }

        public string FieldName { get; set; }

        public ItemWrapper Item { get; set; }
    }
}