﻿namespace FoundationForSitecore.Models
{
    using System.Collections.Generic;

    public class OrbitViewModel
    {
        public IEnumerable<OrbitSlideViewModel> Slides { get; set; }
    }
}