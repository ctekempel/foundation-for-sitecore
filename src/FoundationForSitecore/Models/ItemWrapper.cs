﻿namespace FoundationForSitecore.Models
{
    using Sitecore.Data.Items;

    public class ItemWrapper
    {
        public ItemWrapper()
        {
        }

        public ItemWrapper(Item wrappedItem)
        {
            this.WrappedItem = wrappedItem;
        }

        public Item WrappedItem { get; set; }
    }
}
