﻿namespace FoundationForSitecore.Tests.Unit
{
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using ASP;

    using Fizzler.Systems.HtmlAgilityPack;

    using FluentAssertions;

    using FoundationForSitecore.Models;

    using HtmlAgilityPack;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using RazorGenerator.Testing;

    [TestClass]
    public class OrbitTests
    {
        #region Public Methods and Operators

        [TestMethod]
        public void ContentSlideShouldNotRenderFieldEditorWhenInEditModeWhenFieldNameNotProvided()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel { Item = new ItemWrapper() } } };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode contentDiv = slide.QuerySelector(".orbit-content");
            contentDiv.Should().BeNull();
        }

        [TestMethod]
        public void ContentSlideShouldNotRenderFieldEditorWhenInEditModeWhenItemNotProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    ContentFieldName =
                                                        "some content field"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode contentDiv = slide.QuerySelector(".orbit-content");
            contentDiv.Should().BeNull();
        }

        [TestMethod]
        public void ContentSlideShouldRenderDetails()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    Content =
                                                        new HtmlString(
                                                        "<h1>Some Rich Content</h1>")
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode content = slide.QuerySelector("div");
            content.Should().NotBeNull();
            content.InnerHtml.Trim().Should().Be("<h1>Some Rich Content</h1>");
        }

        [TestMethod]
        public void ContentSlideShouldRenderDetailsInEditModeIfContentProvidedAndItemDetailsNotProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    Content =
                                                        new HtmlString(
                                                        "<h1>Some Rich Content</h1>")
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode content = slide.QuerySelector("div");
            content.Should().NotBeNull();
            content.InnerHtml.Trim().Should().Be("<h1>Some Rich Content</h1>");
        }

        [TestMethod]
        public void ContentSlideShouldRenderFieldEditorWhenInEditModeWithItemDetailsProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    Item = new ItemWrapper(),
                                                    ContentFieldName =
                                                        "some field name"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode contentDiv = slide.QuerySelector(".orbit-content");
            HtmlNode contentFieldEditor = contentDiv.QuerySelector("partial");
            contentFieldEditor.Should().NotBeNull("Partial for field editor should be rendered instead of content");
        }

        [TestMethod]
        public void ImageSlideShouldHandleImageWithNoAltText()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[] { new OrbitSlideViewModel { ImageSrc = "/some-image.jpeg" } }
                            };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().HaveCount(1, "There should only be one image per slide");

            HtmlNode image = images.First();
            image.Attributes["alt"].Should().BeNull("alt attribute should not exist if alt text not specified");
        }

        [TestMethod]
        public void ImageSlideShouldNotRenderFieldEditorWhenInEditModeWhenFieldNameNotProvided()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel { Item = new ItemWrapper() } } };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().BeEmpty("No image tag should be rendered");
            HtmlNode imageFieldEditor = node.QuerySelector("ul li > partial");
            imageFieldEditor.Should()
                .BeNull("Partial for field editor should not be rendered when a field name is not provided");
        }

        [TestMethod]
        public void ImageSlideShouldNotRenderFieldEditorWhenInEditModeWhenItemNotProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel { ImageFieldName = "some image field" }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().BeEmpty("No image tag should be rendered");
            HtmlNode imageFieldEditor = node.QuerySelector("ul li > partial");
            imageFieldEditor.Should()
                .BeNull("Partial for field editor should not be rendered when an item is not provided");
        }

        [TestMethod]
        public void ImageSlideShouldRenderDetails()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    ImageSrc = "/some-image.jpeg",
                                                    ImageAlt = "Some Image"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().HaveCount(1, "There should only be one image per slide");

            HtmlNode image = images.First();
            image.Attributes["src"].Value.Should().Be("/some-image.jpeg");
            image.Attributes["alt"].Value.Should().Be("Some Image");
        }

        [TestMethod]
        public void ImageSlideShouldRenderDetailsInEditModeIfContentProvidedAndItemDetailsNotProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    ImageSrc = "/some-image.jpeg",
                                                    ImageAlt = "Some Image"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().HaveCount(1, "There should only be one image per slide");

            HtmlNode image = images.First();
            image.Attributes["src"].Value.Should().Be("/some-image.jpeg");
            image.Attributes["alt"].Value.Should().Be("Some Image");
        }

        [TestMethod]
        public void ImageSlideShouldRenderFieldEditorWhenInEditModeWithItemDetailsProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    Item = new ItemWrapper(),
                                                    ImageFieldName = "some image field"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            var images = slide.QuerySelectorAll("img");
            images.Should().BeEmpty("No image tag should be rendered");
            HtmlNode imageFieldEditor = slide.QuerySelector("partial");
            imageFieldEditor.Should().NotBeNull("Partial for field editor should be rendered instead of an image");
        }

        [TestMethod]
        public void ShouldNotRenderDeepLinkTagetsForSlidesWhenNotSet()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel() } };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode list = node.QuerySelector("ul");

            HtmlNode slide = list.QuerySelector("li");
            slide.Should().NotBeNull("Should render li elements representing slides");
            slide.Attributes["data-orbit-slide"].Should().BeNull("Should not render link target when not provided");
        }

        [TestMethod]
        public void ShouldRenderDeepLinkTagetsForSlidesWhenSet()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    DeepLinkTarget =
                                                        "Slide Target Name"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode list = node.QuerySelector("ul");

            HtmlNode slide = list.QuerySelector("li");
            slide.Should().NotBeNull("Should render li elements representing slides");
            slide.Attributes["data-orbit-slide"].Should().NotBeNull("Should render link target when not provided");
            slide.Attributes["data-orbit-slide"].Value.Should().Be("Slide Target Name");
        }

        [TestMethod]
        public void ShouldRenderSlides()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel(), new OrbitSlideViewModel(),
                                            new OrbitSlideViewModel()
                                        }
                            };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode list = node.QuerySelector("ul");
            list.Should().NotBeNull();
            list.Attributes.Should().Contain(attribute => attribute.Name == "data-orbit");

            var slides = list.SelectNodes("li");
            slides.Should().NotBeEmpty("Should render li elements representing slides");
            slides.Count.Should().Be(3);
        }

        [TestMethod]
        public void SlideWithCaptionShouldNotRenderFieldEditorWhenInEditModeWhenFieldNameNotProvided()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel { Item = new ItemWrapper() } } };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode captionFieldEditor = slide.QuerySelector(".orbit-caption partial");
            captionFieldEditor.Should()
                .BeNull("Partial for field editor should not be rendered as field name was not provided");
        }

        [TestMethod]
        public void SlideWithCaptionShouldNotRenderFieldEditorWhenInEditModeWhenItemNotProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    CaptionFieldName =
                                                        "Some Field Name"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode captionFieldEditor = slide.QuerySelector(".orbit-caption partial");
            captionFieldEditor.Should()
                .BeNull("Partial for field editor should not be rendered as item was not provided");
        }

        [TestMethod]
        public void SlideWithCaptionShouldRenderDetails()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel { Caption = "Some Caption" } } };

            var node = GetDocumentNode(model, "Normal");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode caption = slide.QuerySelector(".orbit-caption");
            caption.Should().NotBeNull();
            caption.InnerText.Should().Be("Some Caption");
        }

        [TestMethod]
        public void SlideWithCaptionShouldRenderDetailsInEditModeIfContentProvidedAndItemDetailsNotProvided()
        {
            var model = new OrbitViewModel { Slides = new[] { new OrbitSlideViewModel { Caption = "Some Caption" } } };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode caption = slide.QuerySelector(".orbit-caption");
            caption.Should().NotBeNull();
            caption.InnerText.Should().Be("Some Caption");
        }

        [TestMethod]
        public void SlideWithCaptionShouldRenderFieldEditorWhenInEditModeWithItemDetailsProvided()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    Item = new ItemWrapper(),
                                                    CaptionFieldName = "Some field"
                                                }
                                        }
                            };

            var node = GetDocumentNode(model, "Edit");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode captionFieldEditor = slide.QuerySelector(".orbit-caption partial");
            captionFieldEditor.Should().NotBeNull("Partial for field editor should be rendered instead of the caption");
        }

        [TestMethod]
        public void SlideWithoutCaptionShouldNotRenderCaption()
        {
            var model = new OrbitViewModel
                            {
                                Slides =
                                    new[]
                                        {
                                            new OrbitSlideViewModel
                                                {
                                                    ImageSrc = "/some-image.jpeg",
                                                    ImageAlt = "Some Image"
                                                }
                                        }
                            };
            var node = GetDocumentNode(model, "Normal");

            HtmlNode slide = node.QuerySelector("ul li");
            slide.Should().NotBeNull();

            HtmlNode caption = slide.QuerySelector(".orbit-caption");
            caption.Should().BeNull();
        }

        #endregion

        #region Methods

        private static HtmlNode GetDocumentNode(OrbitViewModel model, string pageMode)
        {
            WebViewPage<OrbitViewModel> view = new _Views_Shared__Orbit_cshtml();
            view.ViewBag.PageMode = pageMode;
            HtmlDocument doc = view.RenderAsHtml(model);
            var node = doc.DocumentNode;
            return node;
        }

        #endregion
    }
}